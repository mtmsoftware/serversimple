from bottle import post, get, request, run, static_file
from time import strftime
import os

@post('/')
def receivedata():
    body = request.params
    print(body.value1)

@post('/json')
def receiveJson():
    data = request.json["username"]
    print(data)

@post('/img')
def receiveImg():
    timestump = strftime('%m%d%H%M%S')
    upload   = request.body
    
    print(upload)
    print(type(upload))

    with open("/tmp/img/" + timestump + ".jpeg", 'wb') as f:
      f.write(upload.read())

    return "" + timestump + ".jpeg" + "\r\n"

@get('/get/<filename:re:.*\.jpeg>')
def send_image(filename):
    return static_file(filename, root='/tmp/img', mimetype='image/jpeg')

@get('/get/recentPic')
def send_recent_image():
  logdir='/tmp/img'
  logfiles = sorted([f for f in os.listdir(logdir)])    
  return static_file(logfiles[-1], root='/tmp/img', mimetype='image/jpeg')

run(host='0.0.0.0', port=8080)
